package k8sstr

import (
	"sync"
)

type K8SStatus struct {
	Phase string
}

type namespaceFlag map[string]bool

type NamespaceMap struct {
	lock         sync.RWMutex
	namespaceMap namespaceFlag
}

func (ns *NamespaceMap) Init() {
	ns.namespaceMap = make(namespaceFlag)
}

func (ns *NamespaceMap) Has(name string) bool {
	ns.lock.RLock()
	defer ns.lock.RUnlock()
	return ns.namespaceMap[name]
}

func (ns *NamespaceMap) Delete(name string) {
	ns.lock.Lock()
	defer ns.lock.Unlock()
	delete(ns.namespaceMap, name)
}

func (ns *NamespaceMap) Add(name string) {
	ns.lock.Lock()
	defer ns.lock.Unlock()
	ns.namespaceMap[name] = true
}
